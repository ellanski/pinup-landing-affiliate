let data = {tabs: {
    logOrReg: [
        {
            icon: 'fas fa-sign-in-alt',
            title: 'Вход',
            target: 'loginform'
        },
        {
            icon: 'fas fa-user-plus',
            title: 'Регистрация',
            target: 'regform'
        }
    ]
}};
