export default function initTabs() {
    let tabs = document.querySelectorAll('.js-tab');

    Array.from(tabs).forEach(tab => {
        tab.addEventListener('click', function (event) {
            setActiveTab(tab);
        });
    });

    function setActiveTab(el) {
        let content = document.getElementById(el.dataset.tab);
        let container = findAncestor(content, 'tabs-container');
        let tab = document.querySelector(`.tab[data-tab='${el.dataset.tab}']`);

        Array.from(container.querySelectorAll('.tab, .tab-content')).forEach(
            item => {
                item.classList.remove('is-active');
            }
        );

        content.classList.add('is-active');
        if (tab) tab.classList.add('is-active');
    }

    function findAncestor(el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls));
        return el;
    }
}
