let data = { header: {
    sportsBetting: {
        ru: ['Ставки ', 'на спорт'],
        en: ['Sports ', 'betting']
    },
    onlyTheBestGames: {
        ru: ['Только ', 'лучшие игры'],
        en: ['Only ', 'the best games']
    },
    news: {
        ru: ['', 'Новости'],
        en: ['', 'News']
    },
    ourProjects: {
        ru: ['Наши ', 'проекты'],
        en: ['Our ', 'projects']
    },
    cashout: {
        ru: ['Вывод ', 'средств'],
        en: ['Cash', 'out']
    },
    terms: {
        ru: ['Партнёрское ', 'соглашение'],
        en: ['Terms ', '& Conditions']
    },
    faq: {
        ru: ['', 'FAQ'],
        en: ['', 'FAQ']
    }
}};
