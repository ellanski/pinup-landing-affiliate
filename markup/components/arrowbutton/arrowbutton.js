import debounce from 'lodash/debounce';

export default function initArrowButtons() {
    let buttons = document.querySelectorAll('.arrowbutton');
    let timeout = 1500;

    Array.from(buttons).forEach(button => {
        button.addEventListener('mouseover', function (event) {
            debounce(
                function () {
                    button.classList.add('arrowbutton--animated');
                    setTimeout(function () {
                        button.classList.remove('arrowbutton--animated');
                    }, timeout);
                },
                timeout,
                { leading: true }
            )();
        });
    });
}
