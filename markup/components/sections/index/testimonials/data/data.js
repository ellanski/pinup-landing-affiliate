let data = {
    testimonials: {
        testimonials: [
            {
                cite: {
                    ru: 'Куча возможностей. Думаю PinUp — лучший беттинг из всех, что я видел.',
                    en: 'A lot of possibilities. I think PinUp is the best betting I\'ve ever seen.'
                },
                author: 'infoslots.xyz'
            },
            {
                cite: {
                    ru: 'Качественный софт, подробная статистика. В целом впечатления пока положительные. Надеюсь, что мое мнение со временем не изменится!',
                    en: 'Quality software, detailed statistics. In general, the impressions are positive. I hope that my opinion will not change with time!'
                },
                author: 'say10.ini'
            }
        ]
    }
};
