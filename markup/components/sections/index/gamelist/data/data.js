let data = {
    gamelist: {
        games: [
            {
                name: 'Iron Dog Studio',
                img: 'irondog.png'
            },
            {
                name: 'Belatra Games',
                img: 'belatra.png'
            },
            {
                name: 'EGT',
                img: 'egt.png'
            },
            {
                name: 'Endorphina',
                img: 'endorphina.png'
            },
            {
                name: 'Booming-games.com',
                img: 'booming.png'
            },
            {
                name: 'Gameart',
                img: 'gameart.png'
            },
            {
                name: 'Mr. Slotty',
                img: 'mrslotty.png'
            },
            {
                name: 'Yggdrasil',
                img: 'yggdrasil.png'
            },
        ]
    }
}

