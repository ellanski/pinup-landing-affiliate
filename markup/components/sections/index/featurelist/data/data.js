const data = {featurelist: {
    defaults: [
        {
            icon: 'fas fa-chart-pie',
            title: {
                ru: 'Статистика',
                en: 'Statistics'
            },
            subtitle: {
                ru: 'и анализ трафика',
                en: 'and traffic analysis'
            }
        },
        {
            icon: 'fas fa-cogs',
            title: {
                ru: 'Инструменты',
                en: 'Tools'
            },
            subtitle: {
                ru: 'для продвижения',
                en: 'for promoting'
            }
        },
        {
            icon: 'fas fa-users',
            title: {
                ru: 'Трафик',
                en: 'All sources'
            },
            subtitle: {
                ru: 'всех видов',
                en: 'all kinds'
            }
        },
        {
            icon: 'fas fa-thumbs-up',
            title: {
                ru: 'Качество',
                en: 'High quality'
            },
            subtitle: {
                ru: 'и индивидуальный подход',
                en: 'and personal management'
            }
        }
    ],
    second: [
        {
            icon: 'fas fa-hand-pointer',
            title: {
                ru: 'CPA',
                en: 'CPA'
            },
            subtitle: {
                ru: 'Плата за клик',
                en: 'Cost Per Action'
            }
        },
        {
            icon: 'fas fa-dollar-sign',
            title: {
                ru: 'RevShare',
                en: 'RevShare'
            },
            subtitle: {
                ru: 'Revenue Share',
                en: 'Revenue Share'
            }
        },
        {
            icon: 'fas fa-users',
            title: {
                ru: 'Hybrid',
                en: 'Hybrid'
            },
            subtitle: {
                ru: 'CPA + Revshare',
                en: 'CPA + Revshare'
            }
        }
    ]
}};
