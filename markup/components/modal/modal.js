export default function initModals() {
    let buttons = document.querySelectorAll('.js-modal');

    Array.from(buttons).forEach(button => {
        button.addEventListener('click', function (event) {
            document
                .getElementById(button.dataset.modal)
                .classList.toggle('is-active');
            document.body.classList.toggle('is-fixed');
        });
    });
}
