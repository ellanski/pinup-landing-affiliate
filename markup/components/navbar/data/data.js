let data = {
    navbar: {
        defaults: {
            logo: {
                src: 'pinup-partners-logo.svg',
                // src: 'logo_vulkan_Style.svg',
                alt: {
                    ru: 'Pin-up partners',
                    en: 'Pin-up partners'
                }
            },
            navbarStart: [],
            navbarEnd: [
                {
                    text: {
                        ru: 'Главная',
                        en: 'Home'
                    },
                    href: '#home',
                    class: ''
                },
                {
                    text: {
                        ru: 'О проекте',
                        en: 'About'
                    },
                    href: '#about',
                    class: ''
                },
                {
                    text: {
                        ru: 'Проекты',
                        en: 'Projects'
                    },
                    href: '#projects',
                    class: ''
                },
                {
                    text: {
                        ru: 'Контакты',
                        en: 'Contacts'
                    },
                    href: '#contacts',
                    class: ''
                },
                // {
                //     text: {
                //         ru: 'Вход',
                //         en: 'Login'
                //     },
                //     href: '#',
                //     class: 'button is-success js-modal js-tab',
                //     dataModal: 'registration',
                //     dataTab: 'loginform'
                // },
                // {
                //     text: {
                //         ru: 'Регистрация',
                //         en: 'Join now'
                //     },
                //     href: '#',
                //     class: 'button is-danger js-modal js-tab',
                //     dataModal: 'registration',
                //     dataTab: 'regform'
                // }
            ]
        }
    }
};
