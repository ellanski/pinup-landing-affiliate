export default function initNavbar() {
    let burger = document.getElementById('burger');
    let menu = document.getElementById('topnav');

    burger.addEventListener('click', function () {
        burger.classList.toggle('is-active');
        menu.classList.toggle('is-active');
    });

    let menuItems = menu.querySelectorAll('a, .arrowbutton');
    Array.from(menuItems).forEach(button => {
        button.addEventListener('click', function (event) {
            burger.classList.toggle('is-active');
            menu.classList.toggle('is-active');
        });
    });
}
