'use strict';

/*
    This file can be used as entry point for webpack!
 */
import 'babel-polyfill';

import initNavbar from 'components/navbar/navbar.js';
import initSmoothscroll from 'components/smoothscroll/smoothscroll.js';
import initModals from 'components/modal/modal.js';
import initTabs from 'components/tabs/tabs.js';
import initSwiper from 'components/swiper/swiper.js';
import initArrowButtons from 'components/arrowbutton/arrowbutton.js';

// import validateInput from 'components/input/input.js';
// window.validateInput = validateInput;

// import initForms from 'components/forms/forms.js';

// forEach polyfill
if ('NodeList' in window && !NodeList.prototype.forEach) {
    console.info('polyfill for IE11');
    NodeList.prototype.forEach = function (callback, thisArg) {
        thisArg = thisArg || window;
        for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
        }
    };
}

document.addEventListener('DOMContentLoaded', function () {
    initNavbar();
    initSmoothscroll();
    initModals();
    initTabs();
    initSwiper();
    initArrowButtons();
    // initForms();
});
